;;; sparql-test.el --- One line description  -*- lexical-binding: t; -*-

;; Copyright 2022 Christian Gimenez
;;
;; Author: Christian Gimenez
;; Maintainer: Christian Gimenez
;; Version: 0.1.0
;; Keywords: 
;; URL:
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;;; Code:

(provide 'sparql-test)

(require 'sparql-mode)
(require 'csv)
(require 'graphviz-dot-mode)

(defvar emacssem-sparql-url "http://127.0.0.1:8001/query"
  "The URL to make queries.")

(defun emacssem-query (sparql)
  (with-current-buffer (get-buffer-create "*sparql query*")
    (delete-region (point-min) (point-max))    
    (sparql-execute-query sparql emacssem-sparql-url "text/csv" t)
    (replace-string-in-region "\r" "" (point-min) (point-max)) ;; Some cleaning 
    (csv-parse-buffer t)))

(defconst emacssem-load-snippet-query
  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX emacsi: <http:/nubecita.online/emacsonto/example/0.1/>
PREFIX emacs: <http:/nubecita.online/emacsonto/0.1/>

SELECT ?code WHERE {
 ?s a emacs:Snippet; 
     emacs:code ?code ;
     rdfs:label \"%s\" .
} LIMIT 1")

(defun emacssem--string-cleaning (str)
  (string-replace "\"\"" "\"" str))

(defun emacssem-query-snippet (name)
  "Load a snippet with NAME from the KG Store."
  (let ((snippet (cdr (assoc "code"
                             (car (emacssem-query (format emacssem-load-snippet-query
                                                          name)))))))
    (read (emacssem--string-cleaning snippet))))

(defconst emacssem-watch-node-query
  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX emacs: <http:/nubecita.online/emacsonto/0.1/>
PREFIX emacsi: <http:/nubecita.online/emacsonto/example/0.1/>

SELECT * WHERE {
 %s ?p ?o
} LIMIT 10
")

(defun emacssem-query-po (subject-iri)
  "Return predicates and object from SUBJECT-IRI."
  (let ((results (emacssem-query (format emacssem-watch-node-query subject-iri))))
    (mapcar (lambda (tuples)
              (list (cdr (assoc "p" tuples))
                    (cdr (assoc "o" tuples))))
            results)))

(defvar emacssem-prefixes
  '(("http:/nubecita.online/emacsonto/0.1/" . "emacs")
    ("http:/nubecita.online/emacsonto/example/0.1/" . "emacsi")
    ("http://www.w3.org/2000/01/rdf-schema#" . "rdfs")
    ("http://www.w3.org/1999/02/22-rdf-syntax-ns#" . "rdf")))

(defun emacssem-iri-to-prefixed-iri (str)
  (let ((tuple (assoc str emacssem-prefixes #'string-prefix-p)))
    (if tuple
        (format "%s:%s" (cdr tuple)
                (string-replace (car tuple) "" str))
      str)))
  
(defun emacssem-to-prefixed-iri (tuples)
  "Transform TUPLES IRIs into prefixed IRIs.
TUPLES is a list of triples or tuples."
  (mapcar (lambda (tuple)
            (mapcar (lambda (str)
                      (emacssem-iri-to-prefixed-iri str))
                    tuple))
          tuples))

(defun emacssem-watch-node (iri)
  "Show the node in a buffer."
  (interactive "MIRI?")
  (with-current-buffer (get-buffer-create "*sparql node*")
    (delete-region (point-min) (point-max))

    (insert (format "Subject: %s --\n\n" iri))
    (let ((results (emacssem-query-po iri)))
      (dolist (po results)
        (insert (format "%s --> %S\n" (car po) (cadr po)))))
    (switch-to-buffer-other-frame (current-buffer))))


(defun emacssem--dot-node (subject-iri po-tuples)
  "From a subject and their predicate-object tuples, create the dot.
Create a dot formatted string from SUBJECT-IRI and a list of
predicate-objects PO-TUPLES."
  (concat  "digraph {\n"
           (mapconcat (lambda (tuple)
                        (format "  \"%s\" -> \"%s\" [label=\"%s\"];"
                                subject-iri
                                (cadr tuple)
                                (car tuple)))
                      (emacssem-to-prefixed-iri po-tuples) "\n")
           "\n}"))

;; (defun emacssem--create-dot (dot-str)
;;   "Create a dot image from DOT-STR."
;;   (with-temp-file "/tmp/emacssem-dot.dot"
;;     (insert dot-str))
;;     ;; (insert ?\x82))
;;   (with-current-buffer (find-file-noselect "/tmp/emacssem-dot.dot")
;;     (graphviz-compile-command (buffer-file-name))
;;     (find-file "/tmp/emacssem-dot.png")))

;; This implementation supposed to work. But in case it didn't,
;; uncomment the above function.
(defun emacssem--create-dot (dot-str)
  "Create a dot image from DOT-STR."
  (with-temp-file "/tmp/emacssem-dot.dot"
    (insert dot-str)
    (graphviz-dot-preview)))    

(defun emacssem-dot-node (subject-iri)
  "Generate a dot syntax from an IRI node."
  

;;; sparql-test.el ends here
